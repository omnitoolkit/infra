## [1.3.0](https://gitlab.com/omnitoolkit/infra/compare/v1.2.0...v1.3.0) (2023-06-14)


### Features

* add dedicated update playbook ([0912986](https://gitlab.com/omnitoolkit/infra/commit/0912986452f22b4e1d6e0231429e8aec2ba4d466))
* update ansible collection omnitoolkit.general to v2.5.0 ([b7a1a74](https://gitlab.com/omnitoolkit/infra/commit/b7a1a74f95c2092a14e4556c896a174d101f9f44))
* use dc-backup script from role ([efe4261](https://gitlab.com/omnitoolkit/infra/commit/efe4261efee361dada65d9180acefc1586986168))

## [1.2.0](https://gitlab.com/omnitoolkit/infra/compare/v1.1.0...v1.2.0) (2023-06-01)


### Features

* add app playbook and docker compose scripts ([a024e33](https://gitlab.com/omnitoolkit/infra/commit/a024e335e9d407ed162708d5f442c7b6d872044c))
* add cleanup and heartbeat to backup cronjob ([2771b5d](https://gitlab.com/omnitoolkit/infra/commit/2771b5de80a48201c05d84d8f9d275671f968a63))
* add code service to compose file ([b969853](https://gitlab.com/omnitoolkit/infra/commit/b9698530f1ea6568d441d770ed4afbcd2f3a87ec))
* add firewall rule for tailscale ([3920ecd](https://gitlab.com/omnitoolkit/infra/commit/3920ecd6da73ed4a9b016cb261c91755d179ba34))
* add hoxprox_prod group to ansible ([895f572](https://gitlab.com/omnitoolkit/infra/commit/895f5721ad342b731dc1434774525c9790036b3f))
* add inventory entry, vars and playbook for apollo ([9758e76](https://gitlab.com/omnitoolkit/infra/commit/9758e7627bde03611c8aba43c500360af77a7f0a))
* add PATH and logfile to backup cronjob ([3b49917](https://gitlab.com/omnitoolkit/infra/commit/3b49917391ef5b0cbc5d518a74aa78f452683f0d))
* add repository for coder-templates ([7ee9985](https://gitlab.com/omnitoolkit/infra/commit/7ee9985ef585faf493287dbf39d7113152195892))
* change log location in backup cronjob ([082fc49](https://gitlab.com/omnitoolkit/infra/commit/082fc49fbd9feeb55f7931a41a0c37a1ca5bc084))
* move docker_setup role to linux playbook ([7bfb1a2](https://gitlab.com/omnitoolkit/infra/commit/7bfb1a2ea7dcac0db10114b6121741da13a459ec))
* remove repositories for hugotemplate and inlinedocs ([38432d1](https://gitlab.com/omnitoolkit/infra/commit/38432d1fcfe457f3d56d1c0ddd5625741b79601e))
* rename omnitoolkit playbook ([57b09cd](https://gitlab.com/omnitoolkit/infra/commit/57b09cdcc70acabce2c2148e51cf377088b88521))
* rename project ci to gitlab-ci-templates ([f217563](https://gitlab.com/omnitoolkit/infra/commit/f21756302eaa25ca7e817548eaf009aa999bc494))
* rename project general to ansible-collection-general ([facdccb](https://gitlab.com/omnitoolkit/infra/commit/facdccbf73edd878bf7d274853741e7b2a05409d))
* set different prompt colors for prod and test ([1464c40](https://gitlab.com/omnitoolkit/infra/commit/1464c40f8b453f81bd44626a74fb1914930eb9d1))
* split accesslog from main proxy compose file ([1dacd63](https://gitlab.com/omnitoolkit/infra/commit/1dacd631490b80c0858afc24ff7f112e06099206))
* split up hoxprox playbooks ([1f6b3e3](https://gitlab.com/omnitoolkit/infra/commit/1f6b3e38a1df1634734b61f42c763c92520482e3))
* update collection omnitoolkit.general to v2.3.0 ([9a08ab8](https://gitlab.com/omnitoolkit/infra/commit/9a08ab8ec5991794e33c33f9b527bd6cba973729))
* update gitlab-ci-templates to v3.1.1 ([091b151](https://gitlab.com/omnitoolkit/infra/commit/091b15138054ccf9a73b9e31b991d76041cfe9e3))


### Bug Fixes

* after backup, if no services were running, skip start ([9c59268](https://gitlab.com/omnitoolkit/infra/commit/9c59268de4469d5df076a61c5daa635988b40f86))

## [1.1.0](https://gitlab.com/omnitoolkit/infra/compare/v1.0.0...v1.1.0) (2023-02-04)


### Features

* add configuration for hoxprox_test ([8f2fa24](https://gitlab.com/omnitoolkit/infra/commit/8f2fa245c15265f73c7359b086b6b6f132d1b571))
* add hcloud and hoxprox inventories ([7d2363a](https://gitlab.com/omnitoolkit/infra/commit/7d2363a91324a296fc1bcddef43bd7b3a1bef226))
* move collection versioning to requirements.yml ([32fadab](https://gitlab.com/omnitoolkit/infra/commit/32fadab8ee9e89a981d8d9c8c42e2c87b43da79a))
* move gitlab settings to host_vars ([50195f8](https://gitlab.com/omnitoolkit/infra/commit/50195f8879a8b9262ac2a4c0ebaaae7e3760a5d1))
* move gitlab settings to omnitoolkit group ([63fbce3](https://gitlab.com/omnitoolkit/infra/commit/63fbce32dffbe7cabdcb5e63dacbe3b482ce47d8))
* update collection omnitoolkit.general to v1.1.0 ([aac9ae5](https://gitlab.com/omnitoolkit/infra/commit/aac9ae596cc4c34bca834cd55d3c7b5def433838))

## 1.0.0 (2022-11-26)


### Features

* add ansible script to configure gitlab group ([955e7bd](https://gitlab.com/omnitoolkit/infra/commit/955e7bd2f8e4b4d098ca3fe86a47c2f48c9499d7))
* add new gitlab project ci ([1c71ee6](https://gitlab.com/omnitoolkit/infra/commit/1c71ee64b01fca649c905cac50302925c731ee8d))
* add new gitlab project inlinedocs ([5b79301](https://gitlab.com/omnitoolkit/infra/commit/5b79301d55f6e472115647adbdc2d6b942af7ab5))
* add new gitlab project releasebot ([6cde224](https://gitlab.com/omnitoolkit/infra/commit/6cde2240bb4c1071893df3bff674ee8d36035c93))
* add new project general ([a264191](https://gitlab.com/omnitoolkit/infra/commit/a26419118e6eaf9219119bd4c08cd428a2a6c658))
* add new project hugotemplate ([bfd4b53](https://gitlab.com/omnitoolkit/infra/commit/bfd4b53e63ec976d8b76db0e2a3e851a5221107a))
* move gitlab configuration to separate collection ([3bf0404](https://gitlab.com/omnitoolkit/infra/commit/3bf0404b833fcf5133d616cc4f82a4de164f1884))


### Continuous Integration

* add ci job for release ([e124bff](https://gitlab.com/omnitoolkit/infra/commit/e124bffd6ca856f923a047bf00f02177c9f21b22))
